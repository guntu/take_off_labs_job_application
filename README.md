<img src="https://i.imgur.com/8sBC6fi.png" height="400" />

## How to run it?

1. Clone the repo
2. Install dependencies using the `npm install` command
3. Execute `react-native run-android` or `react-native run-ios`
