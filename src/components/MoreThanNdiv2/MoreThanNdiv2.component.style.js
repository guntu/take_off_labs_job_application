import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  resultText: {
    fontWeight: 'bold'
  }
});
