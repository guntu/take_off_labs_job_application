import React, { Component } from 'react';
import { Card, CardItem, Text, Form, Item, Label, Input, Content, Button } from 'native-base';
import PropTypes from 'prop-types';
import styles from './MoreThanNdiv2.component.style';

class MoreThanNdiv2 extends Component {
  render() {
    return (
      <Card>
        <CardItem header bordered>
          <Text>2. Given an array of N natural numbers, find the number that appears more than N/2 times.</Text>
        </CardItem>
        <CardItem>
          <Content>
            <Form>
              <Item>
                <Label>Array = </Label>
                <Input value={this.props.array} onChangeText={this.props.setArray} />
              </Item>
            </Form>
          </Content>
        </CardItem>
        <CardItem header bordered>
          <Content>
            <Button full onPress={this.props.run}>
              <Text>Run</Text>
            </Button>
          </Content>
        </CardItem>
        <CardItem>
          <Text>Result: </Text>
          <Text style={styles.resultText}>{this.props.result}</Text>
        </CardItem>
      </Card>
    );
  }
}

MoreThanNdiv2.propTypes = {
  array: PropTypes.string,
  setArray: PropTypes.func,
  run: PropTypes.func,
  result: PropTypes.number
}

export default MoreThanNdiv2;
