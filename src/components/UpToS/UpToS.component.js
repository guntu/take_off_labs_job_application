import React, { Component } from 'react';
import { Card, CardItem, Text, Form, Item, Label, Input, Content, Button } from 'native-base';
import PropTypes from 'prop-types';
import styles from './UpToS.component.style';

class UpToS extends Component {
  render() {
    return (
      <Card>
        <CardItem header bordered>
          <Text>1. Given an array of N distinct natural numbers, how many pairs of numbers sum up to a given number S?</Text>
        </CardItem>
        <CardItem>
          <Content>
            <Form>
              <Item>
                <Label>Array = </Label>
                <Input
                  value={this.props.array}
                  onChangeText={this.props.setArray}
                />
              </Item>
              <Item>
                <Label>S = </Label>
                <Input
                  value={this.props.S}
                  onChangeText={this.props.setS}
                />
              </Item>
            </Form>
          </Content>
        </CardItem>
        <CardItem header bordered>
          <Content>
            <Button full onPress={this.props.run}>
              <Text>Run</Text>
            </Button>
          </Content>
        </CardItem>
        <CardItem>
          <Text>Result: </Text>
          <Text style={styles.resultText}>{this.props.result}</Text>
        </CardItem>
      </Card>
    );
  }
}

UpToS.propTypes = {
  array: PropTypes.string,
  S: PropTypes.string,
  setArray: PropTypes.func,
  setS: PropTypes.func,
  run: PropTypes.func,
  result: PropTypes.number
}

export default UpToS;
