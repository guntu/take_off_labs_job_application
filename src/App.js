import React, { Component } from 'react';
import { Container, Content, Header, Text, Title } from 'native-base';
import UpToSPage from './pages/UpToS.page';
import MoreThanNdiv2Page from './pages/MoreThanNdiv2.page';

class App extends Component {
  render() {
    return (
      <Container>
        <Header>
          <Title style={styles.title}>Take Off Labs - Job Application</Title>
        </Header>
        <Content padder>
          <UpToSPage />
          <MoreThanNdiv2Page />
        </Content>
      </Container>
    );
  }
}

const styles ={
  title: {
    alignSelf: 'center'
  }
};

export default App;
