import React, { Component } from 'react';
import MoreThanNdiv2 from '../components/MoreThanNdiv2/MoreThanNdiv2.component';

class MoreThanNdiv2Page extends Component {
  state = {
    array: '1 2 1 1 1 2 2',
    result: 1
  }

  setArray = (value) => {
    this.setState({ array: value });
  }

  // O(n*log_2(n)) time complexity
  solve_more_than_n_div_2 = (A) => {
    if (A.length == 1) {
      return A[0];
    }

    A.sort(); // O(n*log_2(n)) time complexity
    let i = 0;
    let nr = 0;
    let nr_of_appearences = 1
    while (i < A.length - 1) {
      nr = A[i + 1];
      if (A[i] === nr) {
        nr_of_appearences++;
      }
      else {
        nr_of_appearences = 1;
      }
      i++;
      if (nr_of_appearences > A.length/2) {
        return nr;
      }
    }
    return -1;
  }

  run = () => {
    let numbers = this.state.array.split(" ");
    for (let i = 0; i < numbers.length; i++) {
      numbers[i] = parseInt(numbers[i]);
    }
    const res = this.solve_more_than_n_div_2(numbers);
    this.setState({ result: res });
  }

  render() {
    return (
      <MoreThanNdiv2
        array={this.state.array}
        setArray={this.setArray}
        run={this.run}
        result={this.state.result}
      />
    );
  }
}

export default MoreThanNdiv2Page;
