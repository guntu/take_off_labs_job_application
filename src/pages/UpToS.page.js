import React, { Component } from 'react';
import UpToS from '../components/UpToS/UpToS.component';

class UpToSPage extends Component {
  state = {
    array: '1 2 3 0',
    S: '3',
    result: 4
  }

  setArray = (value) => {
    this.setState({ array: value });
  }

  setS = (value) => {
    this.setState({ S: value });
  }

  // O(n^2) time complexity
  solve_up_to_S = (A, S) => {
    let nr = 0;
    for (let i = 0; i < A.length; i++) {
      for (let j = i + 1; j < A.length; j++) {
        if (A[i] + A[j] <= S) {
          nr++;
        }
      }
    }
    return nr;
  }

  run = () => {
    const S = parseInt(this.state.S);
    let numbers = this.state.array.split(" ");
    for (let i = 0; i < numbers.length; i++) {
      numbers[i] = parseInt(numbers[i]);
    }
    const res = this.solve_up_to_S(numbers, S);
    this.setState({ result: res });
  }

  render() {
    return (
      <UpToS
        array={this.state.array}
        S={this.state.S}
        setArray={this.setArray}
        setS={this.setS}
        run={this.run}
        result={this.state.result}
      />
    );
  }
}

export default UpToSPage;
